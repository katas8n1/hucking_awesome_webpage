// - There is a line of comment
import java.util.Scanner;
import java.util.Random;


// class - is a template/blueprint for objects(instance)


// David will be the best developer in the world!
//I have just written this phrase
class Main {
    // method/function ->
    // public -> we can read it from any place in the program
    // static -> is a property of Main
    // void -> method won't return anything

    //   This signature - point from where will start our program
    public static void main(String[] args) {
//        Scanner usersMessage = new Scanner(System.in);
//
//        System.out.println("Enter ur name: ");
//
//        String userAnswer = usersMessage.nextLine();
//        System.out.println("USER WROTE: " + userAnswer);

//        !
//        if(!userAnswer.equals("David")) {
//            System.out.println("Huck you!");
//        }
//
//        System.out.println("How old are you?");
//        String userAge = usersMessage.nextLine();
//
//        System.out.println("USER IS :" + userAge);

//        "18" -> 18
//        If you equal strings you should use str.equals("..")
//        if(Integer.parseInt(userAge) > 18) {
//            System.out.println("You've won a ticket to the strip-bar!");
//        }else {
//            System.out.println("You haven't won a ticket to the strip-bar!");
//        }

////        From number to string -> String.valueOf
//        double point = Math.random();
//        System.out.println("[POINT]" + String.valueOf(point * 3).substring(
//                3,5
//        ));
//        NEW CHANGEs
        //    Code of your program should be here

        //        System - is a package of JDK which allows us to call the system
        //        .out- method of System

        //        initialisation
        //        declaration

        //        ; - dot with coma is the end of operation of init/declar, method call/execution

//        System.out.println("I came here to make you great Developer!");
//
//        byte priceOfCandy = 32;
//
//        int favouriteNumber = 23; // [23]
//        short priceOfSmartphoneXiaome = 126;
//        int vertuPrice = 1231235126;
//        long priceOfFabric = 1231226834;
//
//        boolean isAdmin = false;
//        boolean hasLimit = false;
//        boolean hasBroken = false;
////
//        if(hasBroken) {
//            System.out.println("I wanna buy new one!");
//        }
//
//        System.out.println(favouriteNumber); // [23]
//        favouriteNumber += 13; // 23 + 13 => 36
//        int davidsFavouriteNumber = 9;
//
//        int commonFavouriteNumber = favouriteNumber + davidsFavouriteNumber;


//        String name = "David";
//
//        System.out.println(commonFavouriteNumber);
//        System.out.println("Hello");
//        System.out.println("my friend");
//
//        System.out.println("The end of a program");
//
//        System.out.println(favouriteNumber); // 36


//


//        Validation
//        String password = "davidTheBest";
//        String validEmail = "david@icloud.com";
//        String kirsEmail = "k@i.com";
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Hello, please enter your email here: ");
//        // () -> method
//        String email = scanner.nextLine();
//
//        System.out.println("Please enter here ur password");
//        String usersPassword = scanner.nextLine();
//
//
//        if(email.equals(validEmail)) {
//            // Scope
//            if(usersPassword.equals(password)) {
//                System.out.println("Hello, David. Glad to see you here!");
//            }
//        } else if(email.equals(kirsEmail)) {
//            System.out.println("Kirill you mustn't huck Davids account");
//        } else {
//            System.out.println("There is nothing here yet. Try to create account or your email could be invalid");
//        }

        // What is a method
//        Human david = new Human("David",23);
//        david.greeting();

//
//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("How old are you?");
//        String ageOfUser = scanner.nextLine();
//
//        int parsedAgeOfUser = Integer.valueOf(ageOfUser);


//        from 10
//        to 23
//        if(parsedAgeOfUser > 10 && parsedAgeOfUser < 23) {
//            System.out.println("You have just won the car!");
//        } else if(parsedAgeOfUser >= 23 && parsedAgeOfUser < 55) {
//            System.out.println("You have just won the flat!");
//        } else {
//            System.out.println("You haven't won anything!");
//        }

//
//        switch (parsedAgeOfUser) {
//            case 23:
//                System.out.println("You are very young, only 23 yo");
//                break;
//            case 25:
//                System.out.println("You are very young, only 25 yo");
//                break;
//            case 30:
//                System.out.println("You are very young, only 30 yo");
//                break;
//            default:
//                System.out.println("I don't know how old are u!");
//                break;
//        }



//
    }
}

