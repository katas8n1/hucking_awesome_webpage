public class Human {
    public String name;
    public int age;

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greeting() {
        System.out.println("Hello, my name is " + this.name + " I'm " +  this.age + " years old!");
    }
}
